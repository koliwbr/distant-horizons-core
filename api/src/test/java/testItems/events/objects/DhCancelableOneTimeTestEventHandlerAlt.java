package testItems.events.objects;

/**
 * Dummy test event for unit tests.
 *
 * @author James Seibel
 * @version 2023-6-23
 */
public class DhCancelableOneTimeTestEventHandlerAlt extends DhCancelableOneTimeTestEventHandler
{
	// all other code should be a duplicate of the parent
}
