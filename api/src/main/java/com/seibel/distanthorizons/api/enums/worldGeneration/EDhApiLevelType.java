package com.seibel.distanthorizons.api.enums.worldGeneration;

/**
 * SERVER_LEVEL, <br>
 * CLIENT_LEVEL, <br>
 * UNKNOWN <br>
 *
 * @author James Seibel
 * @version 2022-7-13
 * @since API 1.0.0
 */
public enum EDhApiLevelType
{
	SERVER_LEVEL,
	CLIENT_LEVEL,
	UNKNOWN
}
