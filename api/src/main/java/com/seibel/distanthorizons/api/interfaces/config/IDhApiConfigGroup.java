package com.seibel.distanthorizons.api.interfaces.config;

import com.seibel.distanthorizons.coreapi.interfaces.dependencyInjection.IBindable;

/**
 * This interface is just used to organize API config groups so
 * they can be more easily handled together.
 *
 * @author James Seibel
 * @version 9-15-2022
 * @since API 1.0.0
 */
public interface IDhApiConfigGroup extends IBindable
{
	
}
