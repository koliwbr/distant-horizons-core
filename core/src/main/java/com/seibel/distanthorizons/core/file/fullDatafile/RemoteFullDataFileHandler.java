package com.seibel.distanthorizons.core.file.fullDatafile;

import com.seibel.distanthorizons.core.file.structure.AbstractSaveStructure;
import com.seibel.distanthorizons.core.level.IDhLevel;

import java.io.File;

public class RemoteFullDataFileHandler extends FullDataFileHandler
{
	public RemoteFullDataFileHandler(IDhLevel level, AbstractSaveStructure saveStructure) { super(level, saveStructure); }
	
}
