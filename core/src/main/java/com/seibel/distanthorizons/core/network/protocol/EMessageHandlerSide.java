package com.seibel.distanthorizons.core.network.protocol;

/**
 * CLIENT, <br>
 * SERVER, <br>
 */
public enum EMessageHandlerSide
{
	CLIENT,
	SERVER
}
