package com.seibel.distanthorizons.core.render.renderer;

public interface IDebugRenderable
{
	void debugRender(DebugRenderer r);
	
}
