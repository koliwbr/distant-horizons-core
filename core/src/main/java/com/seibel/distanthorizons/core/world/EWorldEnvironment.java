package com.seibel.distanthorizons.core.world;

/**
 * Client_Only,
 * Client_Server,
 * Server_Only
 */
public enum EWorldEnvironment
{
	Client_Only,
	Client_Server,
	Server_Only
}
