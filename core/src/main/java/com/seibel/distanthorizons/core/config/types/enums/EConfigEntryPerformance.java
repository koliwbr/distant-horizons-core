package com.seibel.distanthorizons.core.config.types.enums;

/**
 * What is the performance impact of an entry
 * (default is DONT_SHOW)
 *
 * @author coolGi
 */
public enum EConfigEntryPerformance
{
	NONE,
	VERY_LOW,
	LOW,
	MEDIUM,
	HIGH,
	VERY_HIGH,
	
	DONT_SHOW
}
