package com.seibel.distanthorizons.core.wrapperInterfaces.modAccessor;

public interface IBCLibAccessor extends IModAccessor
{
	/** Sets the BCLib custom fog renderer */
	void setRenderCustomFog(boolean newValue);
	
}
